package com.epam.courses;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

  public static final String ACCOUNT_SID = "ACd9b6c395fbee180f470dde3a0971bbc9";
  public static final String AUTH_TOKEN = "8e5ecc3c860cdd32b4a05541cf713115";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380671489750"),
            new PhoneNumber("+12254354061"), str).create();
  }
}
